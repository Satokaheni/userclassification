import pickle

file = open('dev.txt', 'r')
tweets = {}
for line in file:
    line = line.split('\t')
    id = line[0]
    tweet = ' '.join(line[3:])
    tweet = tweet.replace('\n', '')
    tweet = tweet.replace('\r', '')
    if 'Not Available' not in tweet:
        tweets[id] = tweet

unlabel = {}
labeled = {}

with open('subj_det.txt', 'rb') as f:
    labeled = pickle.load(f)


ids = list(labeled.keys())

i = 0
for id in tweets.keys():
    if id not in ids:
        unlabel[id] = tweets[id]



i = 0
for id in tweets.keys():
    if id not in ids and id not in unlabel.keys():
        i += 1


print(len(unlabel))
print(i)

file = open('subj_unlabel_tweets.txt', 'w')
for id in unlabel.keys():
    file.write(str(id) + ' ' + unlabel[id] + '\n')



with open('subj_unlabel.txt', 'wb') as f:
    pickle.dump(unlabel, f)
