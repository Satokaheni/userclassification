import pickle
import sys

label = {}

with open('subj_unlabel.txt', 'rb') as f:
	label = pickle.load(f)

# list of punc_tags
punc_tags = ['~', ',', 'G']

tags_file = open('pos.txt', 'r')

#remove links since not important
def removeLinks(tag):
	tag = [ (word,pos) for (word,pos) in tag if pos != 'U' ]
	#tag = [ (word,pos) if pos != 'U' else ('link','U') for (word,pos) in tag ]

	return tag

#generalize the names of users to @User
def removeUsers(tag):
	tag = [ (word,pos) for (word,pos) in tag if pos != '@' ]
	#tag = [ (word,pos) if pos != '@' else ('@User', '@') for (word,pos) in tag ]

	return tag

def removeNumerals(tag):
	tag = [ (word,pos) for (word,pos) in tag if pos != '$' ]

	return tag

#read tags input file
tweet_tag = []
tagged_tweets = []

#to keep up with which tweet we are on
count = 0

for line in tags_file:
	if(line != '\n'):
		tweet_tag.append(line)
	else:
		tagged_tweet = []
		for item in tweet_tag:
			split = item.split('\t')
			word, tag = split[0], split[1]
			tagged_tweet.append((word,tag))
		tweet_tag = []
		tagged_tweets.append(tagged_tweet)

tags_file.close()

tweet_text = []
for tweet in tagged_tweets:	
	id = tweet[0][0]
	del tweet[0]
	tags = removeLinks(tweet)
	tags = removeUsers(tags)
	tweet_text = [ (w,t) for (w,t) in tags if t not in punc_tags ]
	try:
		label[id] = tweet_text
	except:
		print(tweet)
		print(id)
		print(tweet_text)
		sys.exit(0)



with open('subj_unlabel.txt', 'wb') as f:
	pickle.dump(label, f)
