# -*- coding: utf-8 -*-
#cat this to the file tweet_text.txt
import unicodedata as ud
import sys
import json
import pickle

file = open(sys.argv[1], 'r', encoding='utf-8')
ec = sys.stdout.encoding

for line in file:
	try:

		tweet = json.loads(line)
		tweet['text'] = ud.normalize('NFC', tweet['text'])
		tweet['text'] = tweet['text'].encode(ec).decode('utf-8', errors='replace')
		tweet['text'] = tweet['text'].replace('[link removed] ', '')
		tweet['text'] = tweet['text'].replace('\n\n', ' ')
		tweet['text'] = tweet['text'].replace('\n', ' ')
		tweet['text'] = tweet['text'].replace('\r', ' ')
		if tweet['text'][:2] != 'RT':
			print(tweet['text'])
	except:
		continue

file.close()
	
