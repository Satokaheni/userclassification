# -*- coding: utf-8 -*-
import unicodedata as ud
import re
import json
import sys
import pickle

ec = sys.stdout.encoding

tweets_text = []
tweets_class = []
tweets_POS = []
tweets_file = open(sys.argv[1], 'r', encoding='utf-8')
tags_file = open(sys.argv[2], 'r', encoding='utf-8')

#list of non word POS tags
non_word_tags = ['#', '@', '~', 'U', 'E', '$', ',']

#list of tags being removed
r_tags = ['@','$', 'U']

#list of punc_tags
punc_tags = ['~', ',', 'G']

#list of tags used to get the emotions joy, anger, sad, fear, surprise, disgust
emotion_tags = ['#joy','#happy','#pleasure','#pissed','#irritated','#annoyed','#mad','#anger','#disappointed','#miserable','#unhappy','#despair','#sorrow','#sad','#depressed','#terrified','#afraid','#frightened','#scary','#fear','#stunned','#amazed','#astounded','#surprise','#shock', '#wtf','#ew','#gross','#loathe','#nausea','#distaste']

#check if hashtag is last part of tweet
def checkend(hashtag, count, tag_list):
	classify = True
	afterhash = False
	hashcount = count
	
	#check if on right tag
	for (word,pos) in tag_list:	
		if (not afterhash) and word == hashtag:
			if hashcount > 1:
				hashcount = hashcount - 1
			else:
				afterhash = True
		#if after the hashtag and the next word is not a actual grammar word
		elif afterhash and not(pos in non_word_tags):
			classify = False
		#if after the hashtag and the next word is an emotion tag
		elif afterhash and word.lower() in emotion_tags:
			classify = False
	
	if classify and not(hashtag.lower() in emotion_tags):
		classify = False

	return classify

def checkEnd(hashtag, tag_list):
	classify = False

	for (w,t) in reversed(tag_list):
		if t not in r_tags and w == hashtag:
			classify = True
			break
		elif t not in r_tags:
			break

	if hashtag.lower() not in emotion_tags:
		classify = False

	return classify

	
#remove the classfication hashtag from the text
def removeClassification(hashtag, count, pos_tags):
	afterhash = False
	tags = []
	hashcount = count
	
	#remove tag from pos_tags
	for (word,pos) in pos_tags:
		if not afterhash:
			if word == hashtag:
				if hashcount > 1:
					tags.append((word,pos))
					hashcount = hashcount - 1
				else:
					afterhash = True
			else:
				tags.append((word,pos))
		else:
			tags.append((word,pos))

	
	return tags
			
	
#remove links since not important
def removeLinks(tag):			
	tag = [ (word,pos) for (word,pos) in tag if pos != 'U' ]
	#tag = [ (word,pos) if pos != 'U' else ('link','U') for (word,pos) in tag ]
			
	return tag

#generalize the names of users to @User
def removeUsers(tag):
	tag = [ (word,pos) for (word,pos) in tag if pos != '@' ]
	#tag = [ (word,pos) if pos != '@' else ('@User', '@') for (word,pos) in tag ]

	return tag

def removeNumerals(tag):
	tag = [ (word,pos) for (word,pos) in tag if pos != '$' ]

	return tag

#get actual classification tag from 6 emotions
def getClassification(hashtag):
	if hashtag in ['joy', 'happy','pleasure']:
		return 0
	elif hashtag in ['mad', 'anger', 'disappointed', 'pissed', 'irritated', 'annoyed',]:
		return 1
	elif hashtag in ['miserable', 'unhappy', 'despair', 'sorrow', 'sad', 'depressed']:
		return 2
	elif hashtag in ['terrified','afraid','frightened','scary','fear']:
		return 3
	elif hashtag in ['stunned','amazed','astounded','surprise','shock', 'wtf']:
		return 4
	elif hashtag in ['ew','gross','loathe','nausea','distaste']:
		return 5
	
#read tags input file
tweet_tag = []
tagged_tweets = []

#to keep up with which tweet we are on
count = 0

for line in tags_file:
	if(line != '\n'):
		tweet_tag.append(line)
	else:
		tagged_tweet = []
		for item in tweet_tag:
			split = item.split('\t')
			word, tag = split[0], split[1]
			word = ud.normalize('NFC', word)
			tag = ud.normalize('NFC', tag)
			tagged_tweet.append((word,tag))
		tweet_tag = []
		tagged_tweets.append(tagged_tweet)

tags_file.close()

tweet_text = []
for tweet in tagged_tweets:
	tags = removeLinks(tweet)
	tags = removeUsers(tags)
	tags = removeNumerals(tags)
	tweet_text.append([ w for (w,t) in tags if t not in punc_tags ])

with open('processedData/rtsLex.txt', 'wb') as f:
	pickle.dump(tweet_text, f)

sys.exit(0)

c = 0

print(len(tagged_tweets))

#read tweets from input and format for classification
for line in tweets_file:
	try:
		tweet = json.loads(line)
		tweet['text'] = ud.normalize('NFC', tweet['text'])
		tweet['text'] = tweet['text'].encode(ec).decode('utf-8', errors='replace')
		tweet['text'] = tweet['text'].replace('[link removed] ', '')
		tweet['text'] = tweet['text'].replace('\n\n', ' ')
		tweet['text'] = tweet['text'].replace('\n', ' ')
		tweet['text'] = tweet['text'].replace('\r', ' ')
		if tweet['text'][:2] != 'RT':
			#increase count
			count = count + 1
			hashtags = [ h['text'].encode(ec).decode('utf-8') for h in tweet['entities']['hashtags'] ]
			hashtag_count = {}
			for hashtag in hashtags:
				#in case of multiples of the same hashtag
				if hashtag in hashtag_count:
					hashtag_count[hashtag] = hashtag_count[hashtag] + 1
				else:
					hashtag_count[hashtag] = 1
							
				#check if classification is correct otherwise not classified and throw away
				#if checkend('#'+hashtag, hashtag_count[hashtag], tagged_tweets[count-1]):
				if checkEnd('#'+hashtag, tagged_tweets[count-1]):
					tags = removeClassification('#'+hashtag, hashtag_count[hashtag], tagged_tweets[count-1])
					tags = removeLinks(tags)
					tags = removeUsers(tags)
					tags = removeNumerals(tags)
					hashtag = getClassification(hashtag.lower())
					tweets_text.append([ word for (word,tag) in tags ])
					tweets_POS.append(tags)
					tweets_class.append(hashtag)
					break
			
			
		
	except:
		continue

tweets_file.close()
tags_file.close()

#write data to files
with open('tweet_text_new.txt', 'wb') as f:
	pickle.dump(tweets_text, f)

with open('tweet_POS_new.txt', 'wb') as f:
	pickle.dump(tweets_POS, f)
	
with open('tweet_classification_new.txt', 'wb') as f:
	pickle.dump(tweets_class, f)
	
