# encoding=utf-8
import pickle

embeddings = {}

for line in open('sswe-u.txt', 'r'):
    line = line.split()
    word = line[0]
    line = line[1:]
    line[-1] = line[-1].split('\n')[0]
    vectors = []
    for i in range(len(line)):
        vectors.append(float(line[i]))

    embeddings[word] = vectors



with open('sswe_embeddings.txt', 'wb', encoding='utf-8') as f:
    pickle.dump(embeddings, f)