# -*- coding: utf-8 -*-
import pickle
import tweepy
import time
import sys

access_token = '453630151-UdyBubdKoI0Jms8Pxe8yQFoj5nzRKG0OaLA9X2tY'
access_secret = '6o1DLKGZxLopy6LzAjvNZRtKWqR9DZjceZ8z6vjW15n4v'
consumer_key = '03gGwCoJ79fkwPaB2NeaP8lYm'
consumer_secret = 'Uy95u1ISUfMOSIrQHqYsEcKo2HMH95za5mUuSZtWyVokfIENFb'


def get_all_tweets(api, sn):
    print('Gathering all tweets of user: ' + sn)
    all_tweets = []
    new_tweets = []

    #make request for tweets of user
    try:
        new_tweets = api.user_timeline(screen_name=sn, count=200)
    except Exception as e:
        if type(e) == tweepy.error.RateLimitError:
            time.sleep(30*60)
            new_tweets = api.user_timeline(screen_name=sn, count=200)
        else:
            return []

    if not new_tweets:
        return []

    all_tweets.extend(new_tweets)

    #get oldest tweet
    oldest = all_tweets[-1].id-1

    while len(new_tweets) > 0:
        try:
            new_tweets = api.user_timeline(screen_name=sn, count=200, max_id=oldest)
        except tweepy.error.RateLimitError:
            time.sleep(30*60)
            new_tweets = api.user_timeline(screen_name=sn, count=200, max_id=oldest)

        all_tweets.extend(new_tweets)
        oldest = all_tweets[-1].id-1

    outtweets = [ tweet.text for tweet in all_tweets ]

    return outtweets

if __name__ == '__main__':
    users = []
    with open('userids.txt', 'rb') as f:
        users = pickle.load(f)

    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_secret)
    api = tweepy.API(auth)
    au = []
    user_info = { id: [] for id in users }
    bad_users = []
    tweet_length = 0
    count = 0
    for i in range(len(users)):

   #     tweets = get_all_tweets(api,users[i])
   #     user_info[users[i]].extend(tweets)
        try:
            user_info[users[i]] = api.get_user(screen_name=users[i])
        except Exception as e:
            print("Rate Limit hit going to sleep")
            time.sleep(30*60)
            continue
        
        if count % 100 == 0:
            print(count)
        count += 1


    with open('user_data.txt', 'wb') as f:
        pickle.dump(user_info, f)
