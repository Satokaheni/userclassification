# encoding=utf8
from nltk.util import *
from nltk import probability
from nltk.corpus import stopwords
from sklearn import svm
from sklearn import cross_validation
from scipy import sparse
import numpy as np
import sys
import pickle

reload(sys)
sys.setdefaultencoding('utf8')

labeled_tweets = {}
lexicons = {}
word_vectors = {}

with open('labeled_tweets.txt', 'rb') as f:
    labeled_tweets = pickle.load(f)

with open('lexicons.txt', 'rb') as f:
    lexicons = pickle.load(f)

with open('sswe_embeddings.txt', 'rb') as f:
	word_vectors = pickle.load(f)

unigram_list = []
tweets = []
tag_list = ['N', 'O', 'S', '^', 'Z', 'L', 'V', 'A', 'R', '!', 'D', 'P', '&', 'T', 'X', 'Y', '#', 'E', ',', '~', 'G', '@', 'U', '$']
labels = []

for id in labeled_tweets.keys():
    tweet, sent, sub = labeled_tweets[id]
    if sent in ['positive', 'negative']:
        tweets.append([ (str(w),t) for w,t in tweet ])
        if sent == 'positive':
            labels.append(1)
#        elif sent == 'neutral':
#            labels.append(0)
        else:
            labels.append(0)

        unigram_list.extend([ str(w).lower() for w,t in tweet ])

all_unigrams = probability.FreqDist(unigram_list)
unigram_features = [ k for k in all_unigrams.keys() if all_unigrams[k] > 1 ]

lil_features = sparse.lil_matrix((len(tweets), (len(unigram_features)) + len(tag_list) + 1 + 100*50))

def create_features(index):
    features = []
    uni_weight = { w: 0 for w in unigram_features }
    pos_tags = { t: 0 for t in tag_list }
    num_all_caps = 0
    num_elongated = 0
    num_elongated_punc = 0
    score = 0
    npos = 0
    nn = 0

    for w,t in tweets[index]:
        if w.lower() in unigram_features:
            uni_weight[w.lower()] += 1

        if w == w.upper():
            num_all_caps += 1

        if t in tag_list:
            pos_tags[t] += 1

        if t in tag_list and len(w) > 1:
            num_elongated_punc += 1

        if t not in ['#', '@', '~', 'E', '$', ',', 'G']:
            char_list = list(w)
            previous = char_list[0]
            track = {}
            track[previous] = 1
            for i in range(len(char_list) - 1):
                if char_list[i + 1] == previous:
                    track[char_list[i + 1]] += 1
                else:
                    track[char_list[i + 1]] = 1

                previous = char_list[i + 1]

            if len([c for c, v in track.items() if v > 2]):
                num_elongated += 1

    embedding = np.zeros((100, 50))

    z = 0
    for w,t in tweets[index]:
        try:
            embedding[z, :] = word_vectors[w]
        except:
            continue

        z += 1

    for w,t in tweets[index]:
        try:
            score += lexicons[w][0]
            npos += lexicons[w][1]
            nn += lexicons[w][2]
        except:
            continue

    features.extend([ v for w,v in uni_weight.items() ])
    features.extend([ v for w,v in pos_tags.items() ])
    #features.append(num_all_caps)
    #features.append(num_elongated)
    #features.append(num_elongated_punc)
    for j in range(len(embedding)):
        features.extend(embedding[j])
    features.append(score)
    #features.append(npos)
    #features.append(nn)

    lil_features[index, 0:] = features

    if index % 1000 == 0:
        print(index)

print('Creating Features')
for i in range(len(tweets)):
    create_features(i)


print('Performing Cross Validation')

clf = svm.LinearSVC()

scores = cross_validation.cross_val_score(clf, lil_features, labels, cv=10)

print('F1: %0.5f, +/-%0.5f' % (scores.mean(), scores.std()))

