# -*- coding: utf-8 -*-
import pickle
import sys
from nltk.stem.porter import PorterStemmer
from gensim import corpora, models, similarities, matutils
from collections import defaultdict
from nltk.corpus import stopwords

ec = sys.stdout.encoding
stemmer = PorterStemmer()
sw = stopwords.words('english')
	
horror = ['#strategy', '#rts', '#sc2', '#4x', '#rtt', '#turnbasedstrategy']   #horror tags


#joy = [ stemmer.stem(w) for w in joy ]
#anger = [ stemmer.stem(w) for w in anger ]
#sadness = [ stemmer.stem(w) for w in sadness ]
#scared = [ stemmer.stem(w) for w in scared ]
#surprise = [ stemmer.stem(w) for w in surprise ]
#disgust = [ stemmer.stem(w) for w in disgust ]

tweet_text = []

#0 = joy, 1 = anger, 2 = sadness, 3 = fear, 4 = surprise, 5 = disgust
emotions = [ 0, 1, 2, 3, 4, 5 ]

#get tweets
with open('processedData/rtsLex.txt', 'rb') as f:
	tweet_text = pickle.load(f)	
	
frequency = defaultdict(int)
for tweet in tweet_text:
	for token in tweet:
		frequency[token] += 1

tweet_text = [[ word.lower() for word in tweet if frequency[word] > 1 ] for tweet in tweet_text ]

unigrams = []
for tweet in tweet_text:
	unigrams.extend(tweet)
unigrams = list(set(unigrams))

dictionary = corpora.Dictionary(tweet_text)

corpus = [ dictionary.doc2bow(tweet) for tweet in tweet_text ]

lsi = models.lsimodel.LsiModel(corpus=corpus, id2word=dictionary, num_topics=6)

termcorpus = list(matutils.Dense2Corpus(lsi.projection.u.T))

index = similarities.MatrixSimilarity(termcorpus)

def printsim(query, word):
	sims = index[query]
	fmt = [ (dictionary[idother], sim) for idother,sim in enumerate(sims) ]
	wsim = [ v for w,v in fmt if w == word ]
	if wsim == []:
		return 0
	else:
		return wsim[0]

horror_index = []

for i in range(len(dictionary)):
	if dictionary[i] in horror:
		horror_index.append((i,dictionary[i]))

horror_lsa = { w: { ew: 0 for i,ew in horror_index } for w in unigrams }

c = 0

print('calculating similarities for words to each emotion')

for w in unigrams:
	for i,ew in horror_index:
		horror_lsa[w][ew] = printsim(termcorpus[i],w)


print('calculating the SO-LSA of each word')

lsa = { w: 0 for w in unigrams }

for w,e in lsa.items():
	lsa[w] = sum(horror_lsa[w].values())

with open('lexicons/rtsLex.txt', 'wb') as f:
	pickle.dump(lsa, f)
