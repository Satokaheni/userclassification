import sys
import pickle

file = open(sys.argv[1], 'r', encoding='utf-8')

user_dict = {}
user = ''
tweets = []
tweet = ''

for line in file:
    l = line.split(':')

    if '||USER|| ' == l[0]:
        if user != '':
            user_dict[user] = tweets
            tweets = []
        user = l[1][1:len(l[1])-1]


    elif '||TEXT|| ' == l[0]:
        if tweet != '':
            tweets.append(tweet)
            tweet = ''

    else:
        tweet += line


with open('user_dict.txt', 'ab') as f:
    pickle.dump(user_dict, f)