import mechanize
import urllib
from bs4 import BeautifulSoup
from nltk import word_tokenize
import sys
import re

twitch_names = {}

search_string = 'https://www.google.com/search?q='+sys.argv[1].replace(' ', '+')

br = mechanize.Browser()
br.set_handle_robots(False)
br.addheaders = [('User-agent', 'chrome')]

htmltext = br.open(search_string).read()
soup = BeautifulSoup(htmltext)
search = soup.findAll('div', attrs={'id': 'search'})
searchtext = str(search[0])

soup1 = BeautifulSoup(searchtext)
list_items = soup1.findAll('li')

for li in list_items:
    soup2 = BeautifulSoup(str(li))
    links = soup2.findAll('a')
    source_link = str(links[0])
    try:
        twitterLink = re.findall(r'twitter.com/\S+', source_link)
        twitterLink = word_tokenize(twitterLink[0])[0]
        twitterLink = twitterLink.split('/')[1]
        if twitterLink not in twitch_names.keys():
            twitch_names[twitterLink] = 0
        twitch_names[twitterLink] += 1
    except:
        continue

print(max(twitch_names, key=twitch_names.get))