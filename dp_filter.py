import sys
import pickle
from nltk.corpus import stopwords


candidate_tags = ['V','A','R']
aspect_tags = ['N','^']
candidate_deps = ['advmod','amod']
trace_deps = ['dobj','iobj','pobj','csubj','nsubj','xsubj']
sw = stopwords.words('english')

def get_candidates(w,d):
    c = []
    for dep,w1,w2 in d:
        if w1 == w:
            if dep in trace_deps:
                c.extend(get_candidates(w2,d))
        if w2 == w:
            if dep in candidate_deps:
                c.append(w1)
                
    return c


def get_verbs(w,d,t):
    c = []
    for word,pos in t:
        if pos in candidate_tags:
            for dep,w1,w2 in d:
                if w2 == w and w1 == word or w2 == word and w1 == w:
                    c.append(word)
                    
    return c


def get_negation(w, d):
    negated = False
    
    for dep,w1,w2 in d:
        if (w1 == w or w2 == w) and dep == 'neg':
            negated = True
            
    return negated

lexicons = []

with open('lexicons.txt', 'rb') as f:
    lexicons = pickle.load(f)


file = open('test_dp.txt', 'r') 
tweets_dep = []
#get dependencies of tweets
level_root = {}
prev = ''
level = 0
tweet_dep = []
i = 0
for line in file:
    if line.split(':')[0] == 'Input' and i > 0:
        tweets_dep.append(tweet_dep)
        level_root = {}
        prev = ''
        level = 0
        tweet_dep = []
    elif line.split(':')[0] == 'Parse':
        i += 1
    else:
        line = line.replace('|', ' ')
        line = line.split('    ')
        length = len(line)-1
        line = line[-1].split('+--')
        length += len(line) - 1
        line = line[-1].split(' ')
        if length > 0:
            w = line[1]
        else:
            w = line[0]
        dep = line[-1].split('\n')[0]
        if length > level:
            level_root[length] = prev
            level = length
            tweet_dep.append((dep,w,prev))
        elif length == level:
            if level != 0:
                tweet_dep.append((dep,w,level_root[level]))
        else:
            level = length
            tweet_dep.append((dep,w,level_root[level]))
        prev = w          
                 
tweets_dep.append(tweet_dep)                 
   
file.close()

#get named entities of tweets
file = open('ner.txt', 'r')

tweets_ent = []
for line in file:
    line = line.replace('\n','')
    line = line.split(' ')
    ent = []
    te = []
    cat = ''
    for w in line:
        w = w.split('/')
        word = w[0]
        e = w[-1]
        if e != 'O':
            e = e.split('-')
            ent.append(word)
        else:
            if ent:
                te.append(ent)
                ent = []
    tweets_ent.append(te)
    

file.close()
file = open('pos.txt', 'r')

tweets = []
tweet = []

for line in file:  
    if line == '\n':
        tweets.append(tweet)
        tweet = []
    else:
        line = line.split('\t')
        tweet.append((line[0],line[1]))
        


file.close()

for i in range(len(tweets)):
    dep = tweets_dep[i]
    tweet = tweets[i]
    entities = tweets_ent[i]
    
    for w,p in tweet:
        if p in aspect_tags:
            exist = False
            for e in entities:
                if w in e:
                    exist = True
            if not exist:
                multi = False
                for d,w1,w2 in dep:
                    if d == 'nn':
                        entities.append([w1,w2])
                        multi = True
                        break
                if not multi:
                    entities.append([w])
    
    tweets_ent[i] = entities


#get candidates
tweets_ac = []
for i in range(len(tweets_ent)):
    entities = tweets_ent[i]
    tweet = tweets[i]
    deps = tweets_dep[i]
    aspect_cand = []
    tweet_aspect = []
    
    for e in entities:
        neg_tag = False
        for ent in e:
            aspect_cand.extend(get_candidates(ent, deps))
            aspect_cand.extend(get_verbs(ent, deps, tweet))
            neg_tag = get_negation(ent,deps)


        tweet_aspect.append((e,list(set(aspect_cand)),neg_tag))
        aspect_cand = []   
        
    tweets_ac.append(tweet_aspect)
    tweet_aspect = []
    

#remove stop words    
for i in range(len(tweets_ac)):
    tweet_ent_ac = tweets_ac[i]
    nc = []
    
    for ent_pair in tweet_ent_ac:
        e = ent_pair[0]
        a = ent_pair[1]
        b = ent_pair[2]
        c = []
        for w in a:
            if w not in sw:
                c.append(w)
                
        nc.append((e,c,b))
        
    tweets_ac[i] = nc
    
    

#assign sentiment
for i in range(len(tweets_ac)):
    tweet_ent_ac = tweets_ac[i]
    np = []
    
    for ent_pair in tweet_ent_ac:
        e = ent_pair[0]
        a = ent_pair[1]
        b = ent_pair[2]
        c = []
        for w in a:
            if w in lexicons.keys():
                pol = lexicons[w][0]
                if b:
                    pol *= -1
                if pol >= 0:
                    c.append('pos')
                else:
                    c.append('neg')
                
        np.append((e,c))
        
    tweets_ac[i] = np

with open('tweet_entity_sent.txt', 'wb') as f:
    pickle.dump(tweets_ac)