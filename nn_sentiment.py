from keras.models import Sequential
from keras.layers import LSTM, Embedding, Activation, Dense, Convolution1D, MaxPooling1D, Dropout, Merge, Flatten
from keras.layers import AveragePooling1D, GRU
from keras.preprocessing import sequence
from sklearn.cross_validation import train_test_split as ttsplit
from keras.utils.np_utils import to_categorical
from sklearn.metrics import f1_score
import pickle
import sys
import numpy as np

np.random.seed(1337)

dim = 50
word_vectors = {}
labeled_tweets = {}
unlabeled_tweets = {}
lexicons = {}

# Embedding
max_features = 40000
maxlen = 90
embedding_size = 128

# Convolution
filter_length = 3
nb_filter = 128
pool_length = 2

# LSTM
lstm_output_size = 200

# Training
batch_size = 50
nb_epoch = 15

with open('labeled_tweets.txt', 'rb') as f:
    labeled_tweets = pickle.load(f)

with open('unlabeled_tweets.txt', 'rb') as f:
    unlabeled_tweets = pickle.load(f)

with open('sswe_embeddings.txt', 'rb') as f:
	word_vectors = pickle.load(f)

with open('lexicons.txt', 'rb') as f:
    lexicons = pickle.load(f)

unigrams = set()
tags = set()
labels = []
train_tweets = []
test_tweets = []
test_labels = []
train_tags = []
test_tags = []
ids = []
lex_tweets = []

i = 0

for id in labeled_tweets.keys():
    tweet,sent,subj = labeled_tweets[id]
    if sent in ['positive', 'negative']:
        if sent == 'positive':
            labels.append(1)
        elif sent == 'neutral':
            labels.append(0)
        else:
            labels.append(0)
        train_tweets.append([ w for w,t in tweet ])
        train_tags.append([ t for w,t in tweet ])
        for w,t in tweet:
            unigrams.add(w)
            tags.add(t)

        lt = []
        for w,t in tweet:
            try:
                lt.append(int(lexicons[w][0]*100))
            except KeyError:
                lt.append(0)
                continue

        lex_tweets.append(lt)

        i += 1


for id in unlabeled_tweets.keys():
    ids.append(id)
    tweet,sent,subj = unlabeled_tweets[id]
#    if subj == 'objective':
#        test_labels.append(0)
#    else:
#        test_labels.append(1)

    test_tweets.append([ w for w,t in tweet ])
    test_tags.append([ t for w,t in tweet ])
    for w,t in tweet:
        unigrams.add(w)
        tags.add(t)

word_dict = {}
tag_dict = {}
i = 1

for w in unigrams:
	word_dict[w] = i
	i += 1

vocab_size = i

i = 1
for t in tags:
    tag_dict[t] = i
    i += 1

embed_tweets_train = []
embed_tweets_test = []
embed_train_tags = []
embed_test_tags = []

for tweet in train_tweets:
    num_tweet = []
    for w in tweet:
        num_tweet.append(word_dict[w])

    embed_tweets_train.append(num_tweet)

for tweet in test_tweets:
    num_tweet = []
    for w in tweet:
        num_tweet.append(word_dict[w])

    embed_tweets_test.append(num_tweet)


for tags in train_tags:
    num_tags = []
    for t in tags:
        num_tags.append(tag_dict[t])

    embed_train_tags.append(num_tags)


for tags in test_tags:
    num_tags = []
    for t in tags:
        num_tags.append(tag_dict[t])

    embed_test_tags.append(num_tags)


embedding_weights = np.zeros((vocab_size,dim))
for word,index in word_dict.items():
    try:
	    embedding_weights[index,:] = word_vectors[word]
    except:
        continue


#labels = to_categorical(labels, 3)

embed_tweets_train = sequence.pad_sequences(embed_tweets_train, maxlen=maxlen)
embed_train_tags = sequence.pad_sequences(embed_train_tags, maxlen=maxlen)
lex_tweets = sequence.pad_sequences(lex_tweets, maxlen=maxlen)

train_tweets = []

for tweet in embed_tweets_train:
    wv_tweet = []
    for w in tweet:
        wv_tweet.append(embedding_weights[w])

    train_tweets.append(wv_tweet)

train_tweets = np.array(train_tweets)


t_train, t_test, tag_train, tag_test, l_train, l_test, labels, y_test = ttsplit(embed_tweets_train, embed_train_tags, lex_tweets, labels, test_size=.2, random_state=42)


#t_train = sequence.pad_sequences(embed_tweets_train, maxlen=maxlen)
#tag_train = sequence.pad_sequences(embed_train_tags, maxlen=maxlen)
#t_test = sequence.pad_sequences(embed_tweets_test, maxlen=maxlen)
#tag_test = sequence.pad_sequences(embed_test_tags, maxlen=maxlen)


####################################################################################


#(X_train, y_train), (X_test, y_test) = imdb.load_data(nb_words=max_features, test_split=0.2)


#X_train = sequence.pad_sequences(X_train, maxlen=maxlen)
#X_test = sequence.pad_sequences(X_test, maxlen=maxlen)

print('Build model...')

left = Sequential()
left.add(Embedding(vocab_size, dim, input_length=maxlen, weights=[embedding_weights]))
left.add(Dropout(0.25))
#left.add(Convolution1D(nb_filter=nb_filter, filter_length=filter_length, border_mode='valid', activation='relu', subsample_length=1))
#left.add(MaxPooling1D(pool_length=pool_length))
#left.add(LSTM(lstm_output_size))

middle = Sequential()
middle.add(Embedding(vocab_size, dim, input_length=maxlen))
middle.add(Dropout(0.25))

right = Sequential()
right.add(Embedding(vocab_size, dim, input_length=maxlen))
right.add(Dropout(0.25))
#right.add(Convolution1D(nb_filter=nb_filter, filter_length=filter_length, border_mode='valid', activation='relu', subsample_length=1))
#right.add(MaxPooling1D(pool_length=pool_length))
#right.add(LSTM(lstm_output_size))


merged = Merge([left, middle, right], mode='concat')



model = Sequential()
#model.add(Embedding(max_features, embedding_size, input_length=maxlen))
#model.add(Embedding(vocab_size, dim, weights=[embedding_weights], input_length=maxlen))
#model.add(Dropout(0.25))
model.add(merged)
model.add(Convolution1D(nb_filter=nb_filter, filter_length=filter_length, border_mode='valid', activation='relu', subsample_length=1))#, input_shape=(maxlen,dim)))
model.add(Convolution1D(nb_filter=nb_filter, filter_length=filter_length, border_mode='valid', activation='relu', subsample_length=1))
model.add(MaxPooling1D(pool_length=pool_length))
model.add(GRU(lstm_output_size, return_sequences=True))
model.add(GRU(lstm_output_size, return_sequences=True))
model.add(MaxPooling1D(pool_length=pool_length))
model.add(Flatten())
model.add(Dense(200))
model.add(Activation('relu'))
model.add(Dropout(.5))
model.add(Dense(1))
model.add(Activation('sigmoid'))
model.compile(loss='binary_crossentropy', optimizer='adadelta', metrics=['accuracy'])

print('Train...')
model.fit([t_train, l_train, tag_train], labels, batch_size=batch_size, nb_epoch=nb_epoch, validation_split=0.2)#, validation_data=(t_test, y_test))
pred = model.predict([t_test, l_test, tag_test], batch_size=batch_size)
score, acc = model.evaluate([t_test, l_test, tag_test], y_test, batch_size=batch_size)
print('Test score:', score)
print('Test accuracy:', acc)

for i in range(len(pred)):
    try:
        pred[i] = (round(pred[i][0],0))#, round(pred[i][1],0), round(pred[i][2],0))
    except:
        print(i)
        print(len(pred))
        print(pred[i])
        sys.exit(0)

print(f1_score(y_test, pred, average='binary'))