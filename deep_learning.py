import pickle
import sys

tweets = []

with open('tweets_train.txt', 'rb') as f:
    tweets = pickle.load(f)

with open('tweets_dev.txt', 'rb') as f:
    tweets.update(pickle.load(f))

with open('tweets_test.txt', 'rb') as f:
    tweets.update(pickle.load(f))


tweet_tag = []
tagged_tweets = []


#remove links since not important
def removeLinks(tag):
	tag = [ (word,pos) for (word,pos) in tag if pos != 'U' ]
	#tag = [ (word,pos) if pos != 'U' else ('link','U') for (word,pos) in tag ]

	return tag

#generalize the names of users to @User
def removeUsers(tag):
	tag = [ (word,pos) for (word,pos) in tag if pos != '@' ]
	#tag = [ (word,pos) if pos != '@' else ('@User', '@') for (word,pos) in tag ]

	return tag

def removeNumerals(tag):
	tag = [ (word,pos) for (word,pos) in tag if pos != '$' ]

	return tag

for line in open('pos.txt', 'r'):
	if(line != '\n'):
		tweet_tag.append(line)
	else:
		tagged_tweet = []
		for item in tweet_tag:
			split = item.split('\t')
			word, tag = split[0], split[1]
			tagged_tweet.append((word,tag))
		tweet_tag = []
		tagged_tweets.append(tagged_tweet)

tagged = {}

for tweet in tagged_tweets:
    id = tweet[-1][0]
    del tweet[-1]
    tweet = removeLinks(tweet)
    tweet = removeUsers(tweet)
    #tweet = removeNumerals(tweet)
    tagged[id] = tweet


for id in tweets.keys():
	if id in tagged.keys():
		#if labeled_tweets[id][2] == 'none':
		#	labeled_tweets[id] = (tagged[id],labeled_tweets[id][1],'subjective')
		#else:
		tweets[id] = (tagged[id],tweets[id][1],tweets[id][2])



with open('tweets.txt', 'wb') as f:
    pickle.dump(tweets, f)