# encoding=utf8
import sys
import pickle

file = open(sys.argv[1], 'r')

lexicons = {}

with open('lexicons.txt', 'rb') as f:
    lexicons = pickle.load(f)


for line in file:
    line = line.split('\t')
    w = line[0]
    score = line[1]
    np = line[2]
    nn = line[3]

    if not w in lexicons.keys():
        lexicons[w] = (score, np, nn)


print(len(lexicons))

with open('lexicons.txt', 'wb') as f:
    pickle.dump(lexicons, f)