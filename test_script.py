import pickle
import sys

users = {}

with open('users.txt', 'rb') as f:
    users = pickle.load(f)

for user, tweets in users.items():
    file = open('userTweets/'+user+'.txt', 'w')
    for tweet in tweets:
        try:
            file.write(tweet)
        except:
            continue

    file.close()